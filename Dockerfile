FROM openjdk:11.0.13-jdk

COPY ee-service/target/ee-service.jar /app/

RUN echo "Europe/London" > /etc/timezone

ENTRYPOINT java -jar /app/ee-service.jar