# ee-service #

## About this repository ##

This repository (https://bitbucket.org/Victoria1245/ee-service/) contains a project which will build a jar that when running will periodically call out to the EE remaining data webpage (http://add-on.ee.co.uk/status).

The purpose of the repo is to take the information from the EE remaining data webpage and send the data to Splunk using a TCP listener so that the data can then be graphed as the user wishes.

## License ##

[LICENSE](https://bitbucket.org/Victoria1245/ee-service/src/trunk/LICENSE)

## How can I run this application? ##

Before running this application, it is recommended to setup your Splunk instance to receive on a TCP input so that the application can start sending data without issues.

### Configuring a Splunk TCP input ###

#### indexes.conf ####

First add an index to your Splunk instance, e.g.

```
[ee_service]
coldPath = $SPLUNK_DB/ee_service/colddb
enableDataIntegrityControl = 0
enableTsidxReduction = 0
homePath = $SPLUNK_DB/ee_service/db
maxTotalDataSizeMB = 80400
thawedPath = $SPLUNK_DB/ee_service/thaweddb
```

#### inputs.conf ####

Then add a TCP input, e.g.

```
[tcp://9520]
connection_host = dns
index = ee_service
sourcetype = json_no_timestamp
```

### Running the ee-service ###

Once you have setup a Splunk index for the data and configured the TCP input, you can then startup the docker container with the ee-service application inside.

Change the Splunk host and port as appropriate and optionally change the ee_cron_frequency to query the EE remaining data endpoint more or less frequently, e.g.

```
docker run \
    -d \
    --restart always \
    --memory=256m \
    -e SPLUNK_HOST=<splunk-host> \
    -e SPLUNK_PORT=9520 \
    --name ee-service \
    vdocker123/ee-service

docker logs -f ee-service
```

To change the ee_cron_frequency, run the docker container with one of these options:

`-e ee_cron_frequency=daily`
OR `-e ee_cron_frequency=hourly` (this is the default if not specified)
OR `-e ee_cron_frequency=thirtyMins`

Although it is possible to override the values above and choose your own polling frequency, I would not recommend calling more frequently than every 30 minutes or you risk being blocked by EE...  Having had only a 4G Internet connection for 3 years, I found that hourly was more than adequate to predict whether we were likely to run out of data in the month.

The above commands will also start tailing the logs of the container, so press `ctrl + c` to stop the tail.

## Simple Splunk Search ##

Once you've started the docker container, you can then go back in to Splunk and see if the data is being received by running the following query `index=ee_service`.  If the docker container is able to successfully call out to get data from the Internet and send it onwards to Splunk, you should see events in the Splunk search.

_Please note that the default polling frequency of the EE remaining data web page is every hour, so you may have to wait until the following hour before you can see any events in Splunk_.
