package uk.co.vsf.home.monitoring.service.ee;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import uk.co.vsf.home.monitoring.service.Application;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { Application.class })
@TestPropertySource(locations = { "classpath:/application.properties", "classpath:/test.properties" })
public class EeServiceSpringTest {

	@Autowired
	private EeService eeService;

	/**
	 * Enable the following test only when connected to the EE mobile data service
	 * otherwise this test will fail.
	 *
	 * Will check that if connected to the EE mobile data service that you can get
	 * your remaining allowance back.
	 */
	@Disabled
	@Test
	public void test() {
		EeDataStatus status = eeService.getCurrentDataStatus();
		assertNotNull(status);

		assertThat(status.toString(), containsString("remaining"));
		assertThat(status.toString(), containsString("GB"));
	}

	@Test
	public void trueIsTrue() {
		assertTrue(true);
	}
}
