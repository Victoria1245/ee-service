package uk.co.vsf.home.monitoring.service.ee;

public interface EeAddonStatus {

	EeDataStatus getCurrentStatus();
}
