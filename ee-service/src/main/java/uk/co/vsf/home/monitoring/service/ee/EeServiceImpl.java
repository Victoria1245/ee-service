package uk.co.vsf.home.monitoring.service.ee;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class EeServiceImpl implements EeService {

	private static final Logger LOGGER_SDTOUT = LogManager.getLogger("stdoutonly");
	private static final Logger LOGGER = LogManager.getLogger(EeServiceImpl.class);

	private final EeAddonStatus eeAddonStatus;

	public EeServiceImpl(@Autowired EeAddonStatus eeAddonStatus) {
		this.eeAddonStatus = eeAddonStatus;
	}

	@Scheduled(cron = "${ee.cron}")
	@Override
	public EeDataStatus getCurrentDataStatus() {
		EeDataStatus status = eeAddonStatus.getCurrentStatus();
		LOGGER_SDTOUT.info("getCurrentDataStatus + " + status);
		LOGGER.info(status);
		return status;
	}
}
