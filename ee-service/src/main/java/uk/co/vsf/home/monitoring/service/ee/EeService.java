package uk.co.vsf.home.monitoring.service.ee;

public interface EeService {

	EeDataStatus getCurrentDataStatus();
}