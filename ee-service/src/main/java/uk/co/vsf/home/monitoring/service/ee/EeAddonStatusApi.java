package uk.co.vsf.home.monitoring.service.ee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class EeAddonStatusApi implements EeAddonStatus {

	private final RestTemplate restTemplate;

	@Value("${url.ee.addons}")
	private String eeAddonsUrl;

	public EeAddonStatusApi(@Autowired RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public EeDataStatus getCurrentStatus() {
		String response = this.restTemplate.getForObject(eeAddonsUrl, String.class);
		return new EeDataStatus(response);
	}
}
